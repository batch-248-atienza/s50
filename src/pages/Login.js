import { useState, useEffect, useContext } from 'react'
import { Form, Button } from 'react-bootstrap'
import { Navigate } from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'

export default function Login(){
    const { user, setUser } = useContext(UserContext)

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [isActive, setIsActive] = useState(false)

    function authenticate(e){
        e.preventDefault()
        fetch('http://localhost:4000/users/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res=>res.json())
        .then(data=>{
            console.log(data)
            if(typeof data.access !== 'undefined'){
                localStorage.setItem('token', data.access)
                retrieveUserDetails(data.access)

                Swal.fire({
                    title: 'Login Successful!',
                    icon: 'success',
                    text: 'Welcome to Booking App of 248!'
                })
            } else {
                Swal.fire({
                    title: 'Authentication Unsuccessful!',
                    icon: 'error',
                    text: 'Check your credentials!'
                })
            }
        })

        const retrieveUserDetails = (token) => {
            fetch('http://localhost:4000/users/details', {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })
            .then(res=>res.json())
            .then(data=>{
                console.log(data)
                setUser({
                    id: data._id,
                    isAdmin: data.isAdmin
                })
            })
        }

        setEmail('')
        setPassword('')
        //alert(`Welcome back, ${email}`)
    }

    useEffect(()=>{
        if(email !== '' && password !== '' ){
            setIsActive(true)
        }else{
            setIsActive(false)
        }
    }, [email, password])

    return (
        (user.id !== null) ?
            <Navigate to='/courses' />
        :
            <>
                <h1>Login</h1>
                <Form onSubmit={e=>authenticate(e)}>
                    <Form.Group className="mb-3" controlId="userEmail">
                        <Form.Label>Email Address</Form.Label>
                        <Form.Control
                            type="email"
                            placeholder="Enter email"
                            value = {email}
                            onChange={e=>setEmail(e.target.value)}
                            required
                        />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="password">
                        <Form.Label>Password</Form.Label>
                        <Form.Control
                            type="password"
                            placeholder="Password"
                            value = {password}
                            onChange={e=>setPassword(e.target.value)}
                            required
                        />
                    </Form.Group>
                    {
                        isActive
                        ? <Button variant="primary" type="submit" id="submitButton">Log In</Button>
                        : <Button variant="primary" type="submit" id="submitButton" disabled>Log In</Button>
                    }
                </Form>
            </>
    )
}