import { useState, useEffect, useContext } from 'react';
import {Form, Button} from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Register(){
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [email, setEmail] = useState('')
    const [mobileNo, setMobileNo] = useState('')
    const [password1, setPassword1] = useState('')
    const [password2, setPassword2] = useState('')
    const [isActive, setIsActive] = useState(false)

    const { user } = useContext(UserContext)

    function registerUser(e){
        e.preventDefault()
        setFirstName('')
        setLastName('')
        setEmail('')
        setMobileNo('')
        setPassword1('')
        setPassword2('')
        alert('Thank you for signing up')
    }

    useEffect(()=>{
        if(firstName !== '' && lastName !== '' && email !== '' && mobileNo !== '' && mobileNo.length === 11 && password1 !== '' && password2 !== '' && password1 === password2){
            setIsActive(true)
        }else{
            setIsActive(false)
        }
    }, [firstName, lastName, email, mobileNo, password1, password2])

    return (
        (user.id !== null) ?
            <Navigate to='/courses' />
        :
            <>
                <h1>Register</h1>
                <Form onSubmit={e=>registerUser(e)}>
                    <Form.Group className="mb-3" controlId="firstName">
                        <Form.Label>First Name</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="Enter First Name"
                            value = {firstName}
                            onChange={e=>setFirstName(e.target.value)}
                            required
                        />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="lastName">
                        <Form.Label>Last Name</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="Enter Last Name"
                            value = {lastName}
                            onChange={e=>setLastName(e.target.value)}
                            required
                        />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="userEmail">
                        <Form.Label>Email Address</Form.Label>
                        <Form.Control
                            type="email"
                            placeholder="Enter email"
                            value = {email}
                            onChange={e=>setEmail(e.target.value)}
                            required
                        />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="mobileNo">
                        <Form.Label>Mobile Number</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="Enter Mobile Number"
                            value = {mobileNo}
                            onChange={e=>setMobileNo(e.target.value)}
                            required
                        />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="password1">
                        <Form.Label>Password</Form.Label>
                        <Form.Control
                            type="password"
                            placeholder="Password"
                            value = {password1}
                            onChange={e=>setPassword1(e.target.value)}
                            required
                        />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="password2">
                        <Form.Label>Re-enter Password</Form.Label>
                        <Form.Control
                            type="password"
                            placeholder="Verify Password"
                            value = {password2}
                            onChange={e=>setPassword2(e.target.value)}
                            required
                        />
                    </Form.Group>

                    {
                        isActive
                        ? <Button variant="primary" type="submit" id="submitButton">Submit</Button>
                        : <Button variant="primary" type="submit" id="submitButton" disabled>Submit</Button>
                    }

                </Form>
            </>
    )
}