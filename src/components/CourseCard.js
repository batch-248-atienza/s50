import { Row, Col, Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom'
//import { useState, useEffect } from 'react';

//useState Hook
    //Hook in React is a kind of tool
    //The useState Hook allows creation and manipulation of states

    //States are a way for React to keep track of any value and then associate it with a component
    //When a state change, React will re-render only the specific component or part of the component that changed (not the entire page or componenets whose states have not changed)

    //Syntax
    //const [state, setState] = useState(default state)

    //state - this is a special kind of variable that React uses to render components when needed
    //state setter - state setters are the only way to change a state's value. By convention, they are named after the state
    //default state - initial value of the state on component mount

export default function CourseCard({courseProp}){
    //const [count, setCount] = useState(0)
    //const [seat, setSeat] = useState(10)
    const {name, description, price, _id} = courseProp

    //function enroll(){
    //    if(seat<10){
    //        setCount(prevValue => prevValue + 1)
    //        setSeat(prevValue => prevValue - 1)
    //    }
    //}

    //useEffect(() => {
    //    if(seat === 0){
    //        alert("Maximum Enrollees Reached!")
    //    }
    //})

    return (
        <Row className="my-3" >
            <Col>
                <Card className="cardHighlight p-3" >
                    <Card.Body>
                        <Card.Title>{name}</Card.Title>
                        <Card.Subtitle>Description:</Card.Subtitle>
                        <Card.Text>{description}</Card.Text>
                        <Card.Subtitle>Price:</Card.Subtitle>
                        <Card.Text>PHP {price}</Card.Text>
                        {/*<Card.Text>Enrollees: {count}</Card.Text>
                        <Card.Text>Seats Available: {seat}</Card.Text>*/}
                        <Button className='bg-primary' as={Link} to={`/courses/${_id}`}>Details</Button>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    );
};